# https://github.com/anki-code/xonsh-cheatsheet

$PATH.append("$HOME/.bin")
$PATH.append("$HOME/.local/bin")
$PATH.append("$HOME/.ghcup/bin") # Haskell
$PATH.append("$HOME/.local/share/coursier/bin") # Scala

aliases["cp"] = "cp -v"
aliases["mv"] = "mv -v"
aliases["less"] = "less -x4RFsX"
aliases["ports"] = "ss --listening --numeric --tcp --udp --processes"
aliases["rfind"] = "sudo find / -iname"
aliases["sshfs"] = 'sshfs -o reconnect,ServerAliveInterval=15,ServerAliveCountMax=3'

$EDITOR = "vim"
aliases["vi"] = "nvim"
aliases["vim"] = "nvim"

aliases["cat"] = "bat"

# ----- Systemd -----
aliases["sys"] = "sudo systemctl"
aliases["syss"] = "sudo systemctl start"
aliases["syst"] = "sudo systemctl status"
aliases["sysr"] = "sudo systemctl restart"
aliases["reboot"] = "systemctl reboot"
aliases["jctl"] = "journalctl -p 3 -xb"
aliases["poweroff"] = "sudo systemctl poweroff"
aliases["lock"] = "sudo loginctl lock-sessions"

if !(which exa):
  aliases["l"] = "exa --icons"
  aliases["ls"] = "exa --icons --long --group-directories-first"
  aliases["lg"] = "exa --icons --long --group-directories-first --git"
  aliases["ll"] = "exa --icons --long --group-directories-first --tree --level 2"
  aliases["la"] = "exa --icons --all --long --group-directories-first"
  aliases["lla"] = "exa --icons --long --all --group-directories-first --tree --level 2"
else:
  aliases["l"] = "ls --color -Fh"
  aliases["ll"] = "ls --color -lFh"
  aliases["la"] = "ls --color -Fha"
  aliases["lla"] = "ls --color -lFha"

if !(which trash):
  TRASHDIR="~/.trash"
  aliases["rm"] = "trash"
elif $(which trash-put):
  aliases["rm"] = "trash-put -v"
else:
  aliases["rm"] = "rm -v"

# fuck it
aliases["shred"] = "\rm -rf"

if !(which dust):
  aliases["du"] = "dust -r"

aliases[":q"] = "exit"

$OSISI_DIR = f"{$HOME}/Proyectos/code/osisi"
def _sis(args):
  """Run build before all sis commands except build"""
  if args[0] != "build":
    ![$OSISI_DIR/.stack-work/dist/x86_64-linux-tinfo6/ghc-9.6.3/build/sis/sis build]

  ![$OSISI_DIR/.stack-work/dist/x86_64-linux-tinfo6/ghc-9.6.3/build/sis/sis @(args)]
aliases["sis"] = _sis

$GOPATH=f"{$HOME}/.local/share/go"
$VI_MODE = True
xontrib load bashisms abbrevs kitty zoxide prompt_starship direnv gitinfo
