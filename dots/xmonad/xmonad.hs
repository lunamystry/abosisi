import DBus qualified as D
import DBus.Client qualified as D
import Wami.Constants
import Wami.Layouts (myLayoutHook, myShowWNameTheme)
import Wami.Polybar (myPolybar)
import Wami.Startup (myStartupHook)
import Wami.Workspaces (myManageHook, myWorkspaces)
import Wami.Keys (workspaceKeys, myKeys)
import XMonad
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks (docks)
import XMonad.Hooks.OnPropertyChange (onXPropertyChange)
import XMonad.Hooks.Rescreen (rescreenHook)
import XMonad.Hooks.StatusBar
import XMonad.Hooks.WindowSwallowing (swallowEventHook)
import XMonad.Layout.ShowWName (showWName')
import XMonad.Util.EZConfig
import XMonad.Util.Hacks as Hacks
import XMonad.Util.NamedScratchpad (scratchpadWorkspaceTag)
import XMonad.Util.WorkspaceCompare

myEventHook = onXPropertyChange "WM_NAME" myManageHook 
    <> Hacks.windowedFullscreenFixEventHook

main :: IO ()
main = do
  dbus <- D.connectSession
  _ <-
    D.requestName
      dbus
      (D.busName_ "org.xmonad.Log")
      [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]
  xmonad . withSB myPolybar . docks . ewmhFullscreen . ewmh $
    def
      { borderWidth = myBorderWidth
      , handleEventHook = myEventHook
      , focusedBorderColor = myFocusColor
      , keys = workspaceKeys
      , layoutHook = showWName' myShowWNameTheme myLayoutHook
      , manageHook = myManageHook
      , modMask = myModMask
      , normalBorderColor = myNormColor
      , startupHook = myStartupHook
      , terminal = myTerminal
      , workspaces = myWorkspaces
      }
      `additionalKeysP` myKeys
