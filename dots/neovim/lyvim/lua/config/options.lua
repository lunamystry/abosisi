-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here
vim.o.swapfile = false -- No swap files, git is a thing
vim.o.undofile = true
vim.o.inccommand = "split" -- preview substitutions

-- LazyVim by default sets this to unnamedplus which:
--    " ALWAYS use the clipboard for ALL operations (instead of interacting with the "+" and/or "*" registers explicitly)"
-- which I don't like. This options needs to have xclip or xsel
vim.o.clipboard = ""

vim.o.guifont = "FiraCode Nerd Font Mono:h12"
