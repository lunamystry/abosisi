return {
  "nvim-orgmode/orgmode",
  config = function()
    require("orgmode").setup({
      org_agenda_files = { "~/LyCloud/ngenzani/**/*" },
      org_default_notes_file = "~/LyCloud/ngenzani/inbox.org",
      org_todo_keywords = { "TODO(t)", "NEXT(n)", "|", "DONE(d)" },
      org_agenda_skip_scheduled_if_done = true,
      org_agenda_skip_deadline_if_done = true,
      mappings = {
        org = {
          org_toggle_checkbox = "cix",
        },
      },
      org_capture_templates = {
        e = {
          description = "Event",
          template = "* %?\n %^{Event date and time}T",
          target = "~/LyCloud/ngenzani/calendar.org",
        },
        s = {
          description = "Shopping",
          template = "- [ ] %?\n",
          target = "~/LyCloud/ngenzani/inbox.org",
          headline = "Shopping",
        },
        g = {
          description = "Gift idea",
          template = "- [ ] %?\n",
          target = "~/LyCloud/ngenzani/inbox.org",
          headline = "Gifts",
        },
      },
    })
  end,
}
