return {
  "mrcjkb/haskell-tools.nvim",
  version = "^3",
  ft = { "haskell", "lhaskell", "cabal", "cabalproject" },
  lazy = false,
  dependencies = {
    "nvim-lua/plenary.nvim",
    "nvim-telescope/telescope.nvim",
    "neovim/nvim-lspconfig",
  },
  config = function()
    vim.g.haskell_tools = {
      hls = {
        -- on_attach = function(client, bufnr)
        --   on_attach(client, bufnr)
        --   local opts = vim.tbl_extend('keep', { noremap = true, silent = true }, { buffer = bufnr, })
        --   -- haskell-language-server relies heavily on codeLenses,
        --   -- so auto-refresh (see advanced configuration) is enabled by default
        --   vim.keymap.set('n', '<space>cl', vim.lsp.codelens.run, opts)
        --   vim.keymap.set('n', '<space>hs', ht.hoogle.hoogle_signature, opts)
        -- end,
        capabilities = vim.lsp.protocol.make_client_capabilities(),
        settings = {
          haskell = {
            formattingProvider = "fourmolu",
            checkProject = true, -- Setting this to true could have a performance impact on large mono repos.
          },
        },
      },
    }
  end,
}
