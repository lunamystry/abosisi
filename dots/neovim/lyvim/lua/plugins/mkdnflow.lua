return {
  "jakewvincent/mkdnflow.nvim",
  rocks = "luautf8",
  config = function()
    require("mkdnflow").setup({
      -- Config goes here; leave blank for defaults
    })
  end,
}
