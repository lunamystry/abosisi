#!/usr/bin/env bash

# will this prevent the package error on  polybar? restart and find out.
sleep 3

export DEFAULT_NETWORK_INTERFACE=$(ip route | grep '^default' | awk '{print $5}')
killall -q polybar
while pgrep -x polybar >/dev/null; do sleep 1; done

export MONITOR=DisplayPort-1

PATH_TO_CONFIG="$HOME/.config/polybar/esmerelda/config.ini"
polybar -c $PATH_TO_CONFIG -r main 2>$HOME/.config/polybar/logs &

# adjust sleep duration as required to ensure polybar is hidden (i.e., lowered) after restart in monocle layout
sleep 3
xdo lower -N "Polybar"
