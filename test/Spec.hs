import Osisi.Output
import Test.Tasty.Hspec

spec_output :: Spec
spec_output = do
  it "1 should be 1" $
    fromEither (Right 2) `shouldBe` Output 1
