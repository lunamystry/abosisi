module Options (
  RunOptions (..),
  parseOptions,
  makeConfiguration,
) where

import Data.Text (Text, words)
import GHC.Generics (Generic)
import Options.Applicative
import Osisi (usisiFromString)
import Osisi.Amabizo.AppConfig (AppConfig (AppConfig), Selected (..), selectedFromString)
import Osisi.Izenzo.Actions (Action (..), actionFromString)
import Osisi.Types (Usisi (..))
import Prelude hiding (words)

data RunOptions where
  RunOptions ::
    { runoptionsActions :: [Action]
    , runoptionsSelected :: Selected
    , runoptionsTrace :: Text
    , runoptionsUsisi :: Usisi
    } ->
    RunOptions
  deriving (Show, Generic)

data BootstrapOptions where
  BootstrapOptions :: {bootstrapoptionsUsisi :: Usisi} -> BootstrapOptions
  deriving (Show, Generic)

data SpawnOptions where
  SpawnOptions :: Text -> SpawnOptions
  deriving (Show, Generic)

makeConfiguration :: RunOptions -> AppConfig
makeConfiguration o = AppConfig traces selected usisi
  where
    traces = words . runoptionsTrace $ o
    selected = runoptionsSelected o
    usisi = runoptionsUsisi o

parseUsisi :: ReadM Usisi
parseUsisi = eitherReader usisiFromString

parseSelected :: ReadM Selected
parseSelected = eitherReader selectedFromString

parseAction :: ReadM Action
parseAction = eitherReader actionFromString

actionParser :: Parser [Action]
actionParser = many (argument parseAction $ metavar "ACTIONS...")

selectedParser :: Parser Selected
selectedParser =
  option parseSelected $
    short 'p'
      <> long "parcel"
      <> metavar "[PARCEL NAMES]"
      <> help "only run for the following parcels"
      <> value All
      <> showDefault

traceParser :: Parser Text
traceParser =
  strOption $
    short 't'
      <> long "trace"
      <> metavar "[PARCEL NAMES]"
      <> help "keep logs for these parcels"
      <> value ""

usisiParser :: Usisi -> Parser Usisi
usisiParser defaultUsisi =
  option parseUsisi $
    short 'u'
      <> long "usisi"
      <> metavar "(suifeng | vin | milim)"
      <> help "run for muphi usisi"
      <> value defaultUsisi
      <> showDefault

opts :: Usisi -> Parser RunOptions
opts u = RunOptions <$> actionParser <*> selectedParser <*> traceParser <*> usisiParser u

parseOptions :: Usisi -> IO RunOptions
parseOptions defaultUsisi = execParser (info (helper <*> opts defaultUsisi) (fullDesc <> header "abosisi - configuring abosisi"))
