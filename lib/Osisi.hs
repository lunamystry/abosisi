{-# LANGUAGE BlockArguments #-}

module Osisi (
  usisiFromString,
  getDefaultUsisi,
  esmerelda,
  milim,
  suifeng,
  vin,
) where

import Network.HostName (getHostName)
import Osisi.Amabizo.Parcel (Parcel (..))
import Osisi.Types (Usisi (..))
import qualified Parcels.Bat as Bat
import qualified Parcels.Chromium as Chromium
import qualified Parcels.Firefox as Firefox
import qualified Parcels.Fonts as Fonts
import qualified Parcels.Git as Git
import qualified Parcels.Haskell as Haskell
import qualified Parcels.I3 as I3
import qualified Parcels.Immich as Immich
import qualified Parcels.Java as Java
import qualified Parcels.Kaze as Kaze
import qualified Parcels.Kde as Kde
import qualified Parcels.Kitty as Kitty
import qualified Parcels.NNN as NNN
import qualified Parcels.Neovim as Neovim
import qualified Parcels.Nginx as Nginx
import qualified Parcels.Nix as Nix
import qualified Parcels.Polybar as Polybar
import qualified Parcels.Printer as Printer
import qualified Parcels.Python as Python
import qualified Parcels.Rofi as Rofi
import qualified Parcels.Rust as Rust
import qualified Parcels.Scala as Scala
import qualified Parcels.Sddm as Sddm
import qualified Parcels.Sisi as Sisi
import qualified Parcels.Starship as Starship
import qualified Parcels.Syncthing as Syncthing
import qualified Parcels.Volumeicon as Volumeicon
import qualified Parcels.Xmonad as Xmonad
import qualified Parcels.Yad as Yad
import qualified Parcels.Zsh as Zsh

-- Actions
--
usisiFromString :: String -> Either String Usisi
usisiFromString "suifeng" = return suifeng
usisiFromString "vin" = return vin
usisiFromString "milim" = return milim
usisiFromString s = Left $ "u" <> s <> " yena asimazi"

getDefaultUsisi :: IO Usisi
getDefaultUsisi = do
  hostname <- getHostName
  case usisiFromString hostname of
    Left _ -> return Osisi.esmerelda
    Right sisi -> return sisi

-- Configurations
--
commonParcels :: [Parcel]
commonParcels =
  [ Sisi.parcel
  , Bat.parcel
  , Git.parcel
  , Haskell.parcel
  , Rust.parcel
  , Kde.parcel
  , Neovim.parcel
  , NNN.parcel
  , Nix.parcel
  , Python.parcel
  , Scala.parcel
  , Yad.parcel
  , Zsh.parcel
  , Starship.parcel
  ]

desktopParcels :: [Parcel]
desktopParcels =
  [ Chromium.parcel
  , Firefox.parcel
  , Fonts.parcel
  , I3.parcel
  , Xmonad.parcel
  , Kaze.parcel
  , Kitty.parcel
  , Polybar.parcel
  , Rofi.parcel
  , Sddm.parcel
  , Volumeicon.parcel
  , Syncthing.parcel
  , Printer.parcel
  ]

esmerelda :: Usisi
esmerelda =
  Usisi
    { name = "Esmerelda"
    , home = "/home/leny/"
    , projectPath = "$HOME/Proyectos/code/osisi/"
    , parcels = commonParcels <> desktopParcels <> [Immich.parcel]
    }

suifeng :: Usisi
suifeng =
  Usisi
    { name = "Suifeng"
    , home = "/home/leny/"
    , projectPath = "$HOME/Proyectos/code/osisi/"
    , parcels = commonParcels <> desktopParcels
    }

milim :: Usisi
milim =
  Usisi
    { name = "Milim"
    , home = "/home/mandla/"
    , projectPath = "$HOME/Proyectos/code/osisi/"
    , parcels = commonParcels <> desktopParcels <> [Java.parcel]
    }

vin :: Usisi
vin =
  Usisi
    { name = "Vin"
    , home = "/home/leny/"
    , projectPath = "$HOME/Projects/osisi/"
    , parcels = commonParcels <> [Nginx.parcel]
    }
