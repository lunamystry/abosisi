module Osisi.Izenzo.Update (update) where

import Osisi.Amabizo.AppConfig (AppConfig (..), selectedParcels)
import Osisi.Amabizo.Commands (Commands)
import qualified Osisi.Amabizo.Parcel as Parcel
import Osisi.Izenzo.Commands (run)
import Osisi.Types (SisError (..), SisSuccess (..))

update :: AppConfig -> IO [Either SisError SisSuccess]
update c = runUpdate . concatMap Parcel.updates . selectedParcels $ c
  where
    runUpdate :: Commands -> IO [Either SisError SisSuccess]
    runUpdate = run (usisi c)
