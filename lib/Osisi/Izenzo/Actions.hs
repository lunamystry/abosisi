module Osisi.Izenzo.Actions (
  run,
  actionFromString,
  Action (..),
) where

import Control.Monad.Extra (concatMapM)
import Osisi.Amabizo.AppConfig (AppConfig (..))
import Osisi.Izenzo.Build (build)
import Osisi.Izenzo.Configure (configure)
import Osisi.Izenzo.Install (install)
import Osisi.Izenzo.Update (update)
import Osisi.Types (SisError (..), SisSuccess (..))
import Prelude hiding (unwords, words)

data Action
  = Configure
  | Update
  | Installation
  | Build
  deriving (Show)

actionFromString :: String -> Either String Action
actionFromString "configure" = return Configure
actionFromString "update" = return Update
actionFromString "install" = return Installation
actionFromString "build" = return Build
actionFromString s = Left $ "uku-" <> s <> " asikwazi"

run :: AppConfig -> [Action] -> IO ()
run c as = runActions >>= showResults
  where
    runActions :: IO [Either SisError SisSuccess]
    runActions = concatMapM runAction as
    runAction :: Action -> IO [Either SisError SisSuccess]
    runAction Configure = configure c
    runAction Update = update c
    runAction Installation = install c
    runAction Build = build (usisi c)
    showResults :: [Either SisError SisSuccess] -> IO ()
    showResults = mapM_ showResult
    showResult :: Either SisError SisSuccess -> IO ()
    showResult (Left e) = print e
    showResult (Right s) = print s
