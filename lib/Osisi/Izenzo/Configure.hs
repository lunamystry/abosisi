module Osisi.Izenzo.Configure (configure) where

import Control.Monad.Extra (concatMapM)
import Osisi.Amabizo.AppConfig (AppConfig (..), Selected (..), selectedParcels)
import Osisi.Amabizo.Config (Config (..), Item (..))
import Osisi.Amabizo.Parcel (configs, rootConfigs)
import Osisi.Izenzo.Commands (ifRoot, run)
import Osisi.Izenzo.Filesystem (cp, rmdir, touch, write)
import qualified Osisi.Izenzo.Template as Template
import qualified Osisi.Izenzo.Zshrc as Zshrc
import Osisi.Types (Output, SisError (..), SisSuccess (..), Usisi (..))

writeRc :: AppConfig -> [Either SisError SisSuccess] -> IO [Either SisError SisSuccess]
writeRc c o =
  if selectedAll (selected c)
    then Zshrc.write (usisi c) o
    else return o
  where
    selectedAll :: Selected -> Bool
    selectedAll All = True
    selectedAll _ = False

configure :: AppConfig -> IO [Either SisError SisSuccess]
configure c = configure' >>= writeRc c
  where
    configure' :: IO [Either SisError SisSuccess]
    configure' = ifRoot (conf rootConfigs) (conf configs)
      where
        conf fn = concatMapM (runConfigure $ usisi c) . concatMap fn . selectedParcels $ c

runConfigure :: Usisi -> Config -> IO [Either SisError SisSuccess]
runConfigure _ EmptyConfig = return [Right . Success $ "no configuration"]
runConfigure u (Config items) = mapM doConfigure items
  where
    doConfigure :: Item -> IO Output
    doConfigure (CopyItems s d) = cp u s d
    doConfigure (RemoveItems d) = rmdir u d
    doConfigure (TouchItems d) = touch u d
    doConfigure (ResetItems d) = rmdir u d >> touch u d
    doConfigure (TemplateItems s d) = Template.writeDhall u s d
    doConfigure (WriteItems c d) = write u c d
    doConfigure (ZshGroups groups) = return . Right . ZshGroupsAdded $ groups
    -- TODO: Figure out how to run commands
    doConfigure (CommandItems cmds) = do
      _ <- run u cmds
      return . Right . Success $ "ran " <> (show . length $ cmds) <> " configure command(s)"
