module Osisi.Amabizo.PathTemplate where

import Data.Text (Text, last, pack)

type PathTemplate = Text

tmp :: PathTemplate
tmp = "/tmp"

(</>) :: PathTemplate -> PathTemplate -> PathTemplate
p1 </> p2 =
  if Data.Text.last p1 == '/'
    then p1 <> p2
    else p1 <> pack "/" <> p2
