module Osisi.Amabizo.Commands where

import Data.Text (Text)
import Osisi.Amabizo.PathTemplate (PathTemplate, tmp)

type CommandTemplate = Text

type Commands = [Command]

data Command where
  Run :: PathTemplate -> CommandTemplate -> Command
  RunAsIs :: PathTemplate -> Text -> Command
  deriving (Show)

command :: Text -> Commands
command c = [Run tmp c]

commandAsIs :: CommandTemplate -> Commands
commandAsIs c = [RunAsIs tmp c]

commandIn :: PathTemplate -> [Command] -> Commands
commandIn d xs = replaceDir <$> xs
  where
    replaceDir :: Command -> Command
    replaceDir (Run _ c) = Run d c
    replaceDir (RunAsIs _ c) = RunAsIs d c
