module Osisi.Amabizo.Path (
  resolve,
  absFile,
  absDir,
  isDir,
  toText,
  AbsFile,
  AbsDir,
  takeFileName,
  takeDirectory,
  toString,
  (</>),
) where

import Data.Text (Text, isSuffixOf, pack, replace, unpack)
import Osisi.Amabizo.PathTemplate (PathTemplate)
import Osisi.Types (Usisi (..))
import System.Path (AbsDir, AbsFile, takeDirectory, takeFileName, toString, (</>))
import qualified System.Path as Path

resolve :: Usisi -> PathTemplate -> Text
resolve u = resolveHome . resolveProject . resolveDots . resolveConfig
  where
    resolveHome = replace "$HOME" (home u) -- Best if resolved last
    resolveProject = replace "$PROJECT" (projectPath u)
    resolveDots = replace "$DOTS" (projectPath u <> "dots/")
    resolveConfig = replace "$XDG_CONFIG" (home u <> ".config/")

absFile :: Usisi -> PathTemplate -> Path.AbsFile
absFile u = Path.normalise . Path.absFile . unpack . resolve u

absDir :: Usisi -> PathTemplate -> Path.AbsDir
absDir u = Path.normalise . Path.absDir . unpack . resolve u

isDir :: Text -> Bool
isDir = isSuffixOf "/"

toText :: Path.AbsFile -> Text
toText = pack . Path.toString
