module Osisi.Amabizo.Parcel where

import Data.Text (Text)
import Osisi.Amabizo.Commands (Commands)
import Osisi.Amabizo.Config (Config (..))
import Osisi.Amabizo.Install (Install (..))

type ParcelName = Text

data ParcelAction where
  ParcelConfig :: Config -> ParcelAction
  ParcelRootConfig :: Config -> ParcelAction
  ParcelUpdate :: Commands -> ParcelAction
  ParcelInstall :: Install -> ParcelAction

data Parcel where
  Parcel :: ParcelName -> [ParcelAction] -> Parcel

configs :: Parcel -> [Config]
configs (Parcel _ u) = filter notEmpty . map config $ u
  where
    config :: ParcelAction -> Config
    config (ParcelConfig c) = c
    config _ = EmptyConfig
    notEmpty :: Config -> Bool
    notEmpty EmptyConfig = False
    notEmpty Config {} = True

rootConfigs :: Parcel -> [Config]
rootConfigs (Parcel _ u) = filter notEmpty $ map config u
  where
    config :: ParcelAction -> Config
    config (ParcelRootConfig c) = c
    config _ = EmptyConfig
    notEmpty :: Config -> Bool
    notEmpty EmptyConfig = False
    notEmpty Config {} = True

updates :: Parcel -> Commands
updates (Parcel _ u) = concatMap cmd u
  where
    cmd :: ParcelAction -> Commands
    cmd (ParcelUpdate c) = c
    cmd _ = []

installs :: Parcel -> [Install]
installs (Parcel _ u) = filter notEmpty $ map install u
  where
    install :: ParcelAction -> Install
    install (ParcelInstall c) = c
    install _ = NoInstall
    notEmpty :: Install -> Bool
    notEmpty NoInstall = False
    notEmpty Install {} = True
