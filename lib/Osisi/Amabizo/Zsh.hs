{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}

module Osisi.Amabizo.Zsh (
  ZshGroup (..),
  alias,
  asLines,
  emptyline,
  export,
  source,
  noline,
) where

import NeatInterpolation
import Data.Text (Text, intercalate, unpack)
import Dhall (FromDhall, Generic, ToDhall)
import Dhall.Deriving

type Line = Text

type Name = Text

data ZshGroup where
  ZshGroup :: Name -> [Line] -> ZshGroup
  deriving stock (Generic)
  deriving
    (FromDhall, ToDhall)
    via Codec (Field (SnakeCase <<< DropPrefix "_")) ZshGroup

instance Show ZshGroup where
  show (ZshGroup _ []) = ""
  show (ZshGroup _ [""]) = "\n"
  show (ZshGroup n lns) = "# --- " <> unpack n <> "\n# -------\n" <> unpack (intercalate "\n" lns) <> "\n"

alias :: Text -> Text -> [Line]
alias n v = ["alias " <> n <> "='" <> v <> "'"]

export :: Text -> Text -> [Line]
export n v = ["export " <> n <> "=" <> v]

source :: Text -> [Line]
source path = [newSource]
  where
    newSource :: Text
    newSource =
      [text|
        [ -s "$path" ] && source "$path"
      |]

noline :: [Line]
noline = []

asLines :: Text -> [Line]
asLines lns = [lns]

emptyline :: [Line]
emptyline = [""]
