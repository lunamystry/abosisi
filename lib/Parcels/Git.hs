module Parcels.Git where

import Osisi.Amabizo.Config (Config (Config), copy, template)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

config :: Config
config =
  Config $
    copy "templates/" "$DOTS/git/" "$XDG_CONFIG/git/"
      <> copy "ignore" "$DOTS/git/" "$XDG_CONFIG/git/"
      <> template "config" "$DOTS/git/" "$XDG_CONFIG/git/"

install :: Install
install = Install packages []
  where
    packages = ["git", "diff-so-fancy", "difftastic", "git-crypt"]

parcel :: Parcel
parcel =
  Parcel "git" [ParcelConfig config, ParcelInstall install]
