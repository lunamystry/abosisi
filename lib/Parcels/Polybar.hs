module Parcels.Polybar where

import Osisi.Amabizo.Config (Config (Config), copy)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

config :: Config
config =
  Config $
    copy "config" "$DOTS/polybar/" "$XDG_CONFIG/polybar/"
      <> copy "launch.sh" "$DOTS/polybar/" "$XDG_CONFIG/polybar/"
      <> copy "scripts/" "$DOTS/polybar/" "$XDG_CONFIG/polybar/"
      <> copy "esmerelda/" "$DOTS/polybar/" "$XDG_CONFIG/polybar/"

install :: Install
install = Install packages []
  where
    packages = ["polybar"]

parcel :: Parcel
parcel =
  Parcel "polybar" [ParcelConfig config, ParcelInstall install]
