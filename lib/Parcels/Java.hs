module Parcels.Java where

import Osisi.Amabizo.Commands (Commands, command)
import Osisi.Amabizo.Config (Config (..), cmd, zshGroup)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))
import Osisi.Amabizo.Zsh (source)

setJava21AsDefault :: Commands
setJava21AsDefault = command "$HOME/.jabba/bin/jabba alias default zulu@1.21"

config :: Config
config =
  Config $
    zshGroup "java" (source "$HOME/.jabba/jabba.sh")
      <> cmd setJava21AsDefault

update :: Commands
update =
  command "$HOME/.local/bin/jdtls --update"

install :: Install
install = Install [] commands
  where
    -- TODO: This should have a precondition
    commands =
      command "$DOTS/java/jabba_install.sh --skip-rc"
        <> command "$DOTS/java/jdtls-launcher_install.sh"
        <> command "$HOME/.jabba/bin/jabba install zulu@1.21"
        <> command "$HOME/.jabba/bin/jabba install zulu@1.8"

parcel :: Parcel
parcel =
  Parcel
    "java"
    [ ParcelConfig config
    , ParcelInstall install
    , ParcelUpdate update
    ]
