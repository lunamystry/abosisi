module Parcels.Neovim where

import Osisi.Amabizo.Commands (Commands, command)
import Osisi.Amabizo.Config (Config (..), copy, reset, zshGroup)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))
import Osisi.Amabizo.PathTemplate (PathTemplate)
import Osisi.Amabizo.Zsh (alias, emptyline, export)

dotsDir :: PathTemplate
dotsDir = "$DOTS/neovim/lazyvim"

configDir :: PathTemplate
configDir = "$XDG_CONFIG/nvim/"

config :: Config
config =
  Config $
    copy "lua/" dotsDir configDir
      <> copy "after/" dotsDir configDir
      <> copy "init.lua" dotsDir configDir
      <> zshGroup
        "neovim"
        ( alias "vi" "nvim"
            <> alias "vim" "nvim"
            <> emptyline
            <> export "MANPAGER" "'nvim +Man!'"
            <> export "MANWIDTH" "999"
        )

update :: Commands
update = command "nvim --headless \"+Lazy! sync\" +qa"

install :: Install
install = Install packages []
  where
    packages =
      [ "neovim"
      , "ctags"
      , "emacs" -- for orgmode
      , "luarocks"
      , "markdownlint-cli2"
      , "npm"
      , "pandoc" -- for orgmode
      , "python-jsbeautifier"
      , "python-pynvim"
      , "shfmt"
      , "taplo-cli" -- for TOML
      , "texlive-meta"
      , "tree-sitter-cli"
      , "xclip" -- for clipboard
      , "yamllint"
      , "nixfmt"
      , "lazygit"
      ]

parcel :: Parcel
parcel =
  Parcel
    "neovim"
    [ ParcelConfig config
    , ParcelUpdate update
    , ParcelInstall install
    ]
