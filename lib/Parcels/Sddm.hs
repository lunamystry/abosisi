module Parcels.Sddm where

import Osisi.Amabizo.Config
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

priviledged :: Config
priviledged = Config $ template "sddm.conf" "$DOTS/sddm/" "/etc/"

install :: Install
install = Install packages []
  where
    packages =
      [ "sddm"
      , "sddm-config-editor-git"
      ]

parcel :: Parcel
parcel =
  Parcel
    "sddm"
    [ ParcelRootConfig priviledged
    , ParcelInstall install
    ]
