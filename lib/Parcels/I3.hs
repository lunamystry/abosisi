module Parcels.I3 where

import Osisi.Amabizo.Commands (Commands, command)
import Osisi.Amabizo.Config (Config (Config), copy, template)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))
import Osisi.Amabizo.PathTemplate (PathTemplate)

dotsDir :: PathTemplate
dotsDir = "$DOTS/i3/"

configDir :: PathTemplate
configDir = "$XDG_CONFIG/i3/"

update :: Commands
update = command "i3 reload"

config :: Config
config =
  Config $
    template "config" dotsDir configDir
      <> copy "i3blocks.conf" dotsDir configDir
      <> copy "keybindings" dotsDir configDir
      <> copy "scripts/" dotsDir configDir

install :: Install
install = Install packages []
  where
    packages =
      [ "i3-wm"
      , "i3lock"
      , "i3status"
      , "i3blocks"
      , "flameshot" -- for screenshots
      , "variety" -- for wallpaper
      , "unclutter" -- Hide the mouse pointer if unused for a duration
      , "picom" -- transparent terminal
      , "feh" -- viewing images
      , "rofi-greenclip" -- clipboard that integrated with rofi
      , "kdeconnect"
      , "inkscape"
      , "rts_bpp-dkms-git" -- A kernel module for realtek card reader
      , "arcolinux-logout" -- a pretty logout script thingy
      ]

parcel :: Parcel
parcel =
  Parcel "i3" [ParcelConfig config, ParcelInstall install]
