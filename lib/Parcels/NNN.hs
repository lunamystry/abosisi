module Parcels.NNN where

import Osisi.Amabizo.Commands (Commands, command, commandIn)
import Osisi.Amabizo.Config (Config (..), zshGroup)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))
import Osisi.Amabizo.PathTemplate (PathTemplate)
import Osisi.Amabizo.Zsh (alias, export)

optDir :: PathTemplate
optDir = "$HOME/.local/opt/"

nnnDir :: PathTemplate
nnnDir = optDir <> "/nnn"

clone :: Commands
clone = commandIn optDir $ command "git clone https://github.com/jarun/nnn.git"

pull :: Commands
pull = commandIn nnnDir $ command "git clean -xf && git stash && git pull && (git stash pop || true)"

compile :: Commands
compile = commandIn nnnDir $ command "make O_NERD=1"

moveToBin :: Commands
moveToBin = commandIn nnnDir $ command "cp nnn $HOME/.local/bin"

getPlugins :: Commands
getPlugins =
  commandIn "$XDG_CONFIG/nnn/plugins" (command "sh -c \"$(curl -Ls https://raw.githubusercontent.com/jarun/nnn/master/plugins/getplugs)\"")
    <> commandIn "$XDG_CONFIG/nnn" (command "rm plugins-*.tar.gz")

update :: Commands
update = pull <> compile <> moveToBin <> getPlugins

install :: Install
install = Install [] commands
  where
    commands = clone <> compile <> moveToBin <> getPlugins

config :: Config
config =
  Config $
    zshGroup
      "nnn"
      ( export "NNN_PLUG" "'f:finder;o:fzopen;p:preview-tui'"
          <> export "NNN_FIFO" "/tmp/nnn.fifo"
          <> alias "nnn" "nnn -a -P p"
      )

parcel :: Parcel
parcel =
  Parcel
    "nnn"
    [ ParcelConfig config
    , ParcelUpdate update
    , ParcelInstall install
    ]
