module Parcels.SSH where

import Osisi.Amabizo.Config (Config (..), copy)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

config :: Config
config = Config $ copy "config" "$DOTS/ssh/" "$HOME/.ssh/"

install :: Install
install = Install packages []
  where
    packages = ["openssh", "sshfs"]

parcel :: Parcel
parcel =
  Parcel
    "bat"
    [ ParcelConfig config
    , ParcelInstall install
    ]
