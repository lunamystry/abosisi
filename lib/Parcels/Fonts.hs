module Parcels.Fonts where

import Osisi.Amabizo.Config (Config (Config), copy)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

priviledged :: Config
priviledged = Config $ copy "vconsole.conf" "$DOTS/fonts/" "/etc/"

install :: Install
install = Install packages []
  where
    packages =
      [ "papirus-icon-theme"
      , "terminus-font" -- Used in /etc/vconsole.conf
      , "ttc-iosevka"
      , "ttf-fira-code"
      , "ttf-anonymouspro-nerd"
      , "ttf-inconsolata-lgc-nerd"
      , "ttf-hack-nerd"
      , "ttf-terminus-nerd"
      , "ttf-iosevkaterm-nerd"
      , "ttf-google-fonts-git"
      ]

parcel :: Parcel
parcel =
  Parcel "fonts" [ParcelRootConfig priviledged, ParcelInstall install]
