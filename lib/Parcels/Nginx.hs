module Parcels.Nginx where

import Osisi.Amabizo.Commands (Commands, command)
import Osisi.Amabizo.Config (Config (..), cmd, copy)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

enablePorts :: Commands
enablePorts =
  command "ufw allow 'SSH'"
    <> command "ufw allow 'WWW Full'"
    <> command "systemctl enable nginx"
    <> command "systemctl start nginx"

config :: Config
config =
  Config $
    copy "nginx.conf" "$DOTS/nginx/" "/etc/nginx"
      <> copy "mandla.me" "$DOTS/nginx/sites-available" "/etc/nginx/sites-available"
      <> cmd enablePorts

install :: Install
install = Install packages []
  where
    packages = ["nginx-mainline", "ufw", "ufw-extras"]

parcel :: Parcel
parcel =
  Parcel
    "nginx"
    [ ParcelRootConfig config
    , ParcelInstall install
    ]
