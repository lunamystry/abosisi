module Parcels.Volumeicon where

import Osisi.Amabizo.Config (Config (Config), copy)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

config :: Config
config = Config $ copy "volumeicon" "$DOTS/volumeicon/" "$XDG_CONFIG/volumeicon/"

install :: Install
install = Install packages []
  where
    packages = ["volumeicon"]

parcel :: Parcel
parcel =
  Parcel "volumneicon" [ParcelConfig config, ParcelInstall install]
