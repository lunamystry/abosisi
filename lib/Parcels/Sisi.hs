{-# LANGUAGE QuasiQuotes #-}

module Parcels.Sisi where

import Data.Text (Text)
import NeatInterpolation (text)
import Osisi.Amabizo.Commands (Commands, command, commandIn)
import Osisi.Amabizo.Config (Config (Config), cmd, zshGroup)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))
import Osisi.Amabizo.Zsh (asLines)

update :: Commands
update =
  command "yay -Syu --noconfirm"
    <> command "sudo updatedb"

config :: Config
config = Config $ zshGroup "sisi" $ asLines sisFn

installYay :: Commands
installYay =
  command "pacman -S base-devel git go"
    <> commandIn "/opt" (command "git clone https://aur.archlinux.org/yay.git")
    <> commandIn "/opt" (command "chown -R {{username}}:{{username}} ./yay")
    <> commandIn "/opt/yay" (command "su {{username}} -c 'makepkg -si'")

priviledged :: Config
priviledged = Config $ cmd installYay

sisFn :: Text
sisFn =
  [text|
    OSISI_DIR=$$HOME/Proyectos/code/osisi

    function sis {
      if [ $$1 != "build" ]
      then
        $$OSISI_DIR/.stack-work/dist/x86_64-linux-tinfo6/ghc-9.4.8/build/sis/sis build
      fi

      $$OSISI_DIR/.stack-work/dist/x86_64-linux-tinfo6/ghc-9.4.8/build/sis/sis "$@"
    }
  |]

install :: Install
install = Install packages []
  where
    packages =
      [ "terminus-font" -- Used in /etc/vconsole.conf
      , "ttc-iosevka"
      ]

parcel :: Parcel
parcel =
  Parcel
    "osisi"
    [ ParcelConfig config
    , ParcelRootConfig priviledged
    , ParcelUpdate update
    , ParcelInstall install
    ]
