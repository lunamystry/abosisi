module Parcels.Kaze where

import Osisi.Amabizo.Commands (command)
import Osisi.Amabizo.Config (Config (Config), copy, template, write)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

config :: Config
config =
  Config $
    copy "rclone.conf" "$DOTS/kaze/" "$XDG_CONFIG/rclone/"
      <> write "# Kaze LOG DIRECTORY" "$XDG_CONFIG/rclone/log/README.md"

priviledged :: Config
priviledged = Config $ template "kaze.service" "$DOTS/kaze/" "/etc/systemd/system"

install :: Install
install = Install packages commands
  where
    packages = ["rclone"]
    commands = command "mkdir -p $HOME/Kaze"

parcel :: Parcel
parcel =
  Parcel
    "kaze"
    [ ParcelConfig config
    , ParcelRootConfig priviledged
    , ParcelInstall install
    ]
