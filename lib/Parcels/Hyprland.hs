module Parcels.Hyprland where

import Osisi.Amabizo.Config (Config (Config), template)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))
import Osisi.Amabizo.PathTemplate (PathTemplate)

dotsDir :: PathTemplate
dotsDir = "$DOTS/hyprland/"

configDir :: PathTemplate
configDir = "$XDG_CONFIG/hypr/"

install :: Install
install = Install packages []
  where
    packages =
      [ "hyprland"
      , "hypridle"
      , "hyprlock"
      , "hyprshot" -- screenshots
      , "waybar" -- status bar
      , "wofi" -- launcher/menu
      , "swaync" -- notification deamon - notification center
      ]

config :: Config
config = Config $ template "config" dotsDir configDir

parcel :: Parcel
parcel = Parcel "hyprland" [ParcelConfig config, ParcelInstall install]
