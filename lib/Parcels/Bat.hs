module Parcels.Bat where

import Osisi.Amabizo.Config (Config (..), template, zshGroup)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))
import Osisi.Amabizo.Zsh (alias)

config :: Config
config =
  Config $
    template "config" "$DOTS/bat/" "$XDG_CONFIG/bat/"
      <> zshGroup
        "bat"
        (alias "cat" "bat")

install :: Install
install = Install packages []
  where
    packages = ["bat", "bat-extras"]

parcel :: Parcel
parcel =
  Parcel
    "bat"
    [ ParcelConfig config
    , ParcelInstall install
    ]
