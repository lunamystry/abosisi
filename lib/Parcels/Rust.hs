module Parcels.Rust where

import Osisi.Amabizo.Commands (Commands, command)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

update :: Commands
update = command "rustup update"

install :: Install
install = Install packages commands
  where
    packages = ["rustup"]
    commands =
      command "rustup default stable"

parcel :: Parcel
parcel = Parcel "rust" [ParcelUpdate update, ParcelInstall install]
