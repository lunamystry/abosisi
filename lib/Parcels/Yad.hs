module Parcels.Yad where

import Osisi.Amabizo.Config (Config (Config), copy)
import Osisi.Amabizo.Install (Install (..))
import Osisi.Amabizo.Parcel (Parcel (..), ParcelAction (..))

config :: Config
config = Config $ copy "yad.conf" "$DOTS/yad/" "$XDG_CONFIG/"

install :: Install
install = Install packages []
  where
    packages = ["yad"]

parcel :: Parcel
parcel =
  Parcel "yad" [ParcelConfig config, ParcelInstall install]
